FROM python:2.7.16-alpine3.9

LABEL description="Clone by eficode/robotframework-selenium images"

ENV SCREEN_WIDTH 1920
ENV SCREEN_HEIGHT 1080
ENV SCREEN_DEPTH 16
ENV DEPS="\
    dbus \
    firefox-esr \
    ttf-freefont \
    xvfb \
    python2-tkinter \
"

COPY requirements.txt /tmp/requirements.txt
COPY entrypoint_pabot.sh /opt/bin/entrypoint_pabot.sh
COPY entrypoint_robot.sh /opt/bin/entrypoint_robot.sh

RUN apk update ;\
    apk add --no-cache ${DEPS} ;\
    pip install --no-cache-dir -r /tmp/requirements.txt ;\
    # Install geckodriver
    wget -q https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz ;\
    tar -C /usr/bin/ -xf geckodriver-*.tar.gz ;\
    # List packages and python modules installed
    apk info -vv | sort ;\
    pip freeze ;\
    # Cleanup
    rm -rf /var/cache/apk/* /tmp/requirements.txt /geckodriver-*.tar.gz

ENTRYPOINT [ "/opt/bin/entrypoint_pabot.sh" ]
